#! /bin/bash
# Jorge D. Pérez López
# 06-03-2024
#
# Sinopsis: M01ex7-jorgeperez.sh dirDesti file...
#
# Descripció: El programa rep un directori com a primer argument i n arxius.
# Si els fitxers existeixen i son fitxers executables binaris, els copia al
# directori, sinó els mostra per stderr.
# ------------------------------------------------------------------------------
ERR_NARGS=1
ERR_DIR=2

if [ $# -lt 2 ] ; then
  echo "Error: El programa ha de tenir mínim 2 arguments."
  echo "Usage: $0 dir file1 file2 ..."
  exit $ERR_NARGS
fi

dir=$1
shift

if ! [ -d $dir ] ; then
  echo "Error: $dir no és un directori."
  echo "Usage: $0 dir file1 file2..."
  exit $ERR_DIR
fi

status=0
err=0

for file in $*
do
  if [ -e $file ] ; then
    iself=$(file -b $file | head -n1 | cut -c1-3)
    if [ $iself = "ELF" ] ; then
      cp $file $dir
      echo $file
    else
      echo "$file" >> /dev/stderr
      status=3
      ((err++))
    fi
  else
    echo "Error: $file no existeix"
  fi
done

echo "Total de fitxers no executale-binaris: $err"

exit $status
