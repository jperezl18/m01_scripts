#! /bin/bash
# Jorge D. Pérez López
# 12-04-2024
#
# Sinopsis: fsize user
#
# Descripció: El programa rep un nom d'usuari com a argument i si existeix,
# mostra l'espai que ocupa el seu directori home.
# ------------------------------------------------------------------------------
function fsize(){
  ERR_USER=2

  user=$1
  
  line=$(grep "^$user:" /etc/passwd)

  if [ -z "$line" ] ; then
    echo "Error: L'usuari $user no existeix."
    return $ERR_USER
  fi

  dirHome=$(echo $line | cut -d: -f6)
  
  du -sh $dirHome

  return 0
}

# Sinopsis: loginargs user1 user2 ...
#
# Descripció: Rep n usuaris com a arguments i si existeixen,
# mostra la ocupació dels seus directoris home.
# ------------------------------------------------------------ 
function loginargs(){
  ERR_NARGS=1

  if [ $# -eq 0 ] ; then
    echo "Error: El programa ha de tenir mínim un argument."
    echo "Usage: loginargs user1 user2 ..."
    return $ERR_NARGS
  fi

  for user in $*
  do
    fsize $user
  done

  return 0
}

# Sinopsis: loginfile file
#
# Descripció: Rep un argument que és un fitxer. cada línia 
# del fitxer té un login.
# -----------------------------------------------------------
function loginfile(){
  ERR_FILE=1

  file=$1

  if [ ! -f $file ] ; then
    echo "Error: $file no existeix o no es un fitxer."
    return $ERR_FILE  
  fi

  while read -r user
  do
    fsize $user
  done < $file

  return 0  
}

# Sinopsis: loginstdin
#
# Descripció: Rep logins per stdin. Per cada un d'ells calcula
# la ocupació del seu home.
# -----------------------------------------------------------
function loginstdin(){
  
  while read -r user
  do
    fsize $user
  done

  return 0
}

# Sinopsis: loginboth / loginboth file

# Descripció: Si rep un fitxer com a argument, processa els logins del fitxer.
# Si no rep res, processa logins per stdin.
# ----------------------------------------------------------------------------
function loginboth(){
  
  if [ $# -eq 1 ] ; then
    file=$1
  else
    file="/dev/stdin"
  fi

  while read -r user
  do
    fsize $user
  done < $file

  return 0

}


