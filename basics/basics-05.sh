#! /bin/bash
# Jorge D. Pérez López
# 19/02/2024 
# Exercicis d'scripts bàsics 5
# 
# Sinopsis: Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
#------------------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -ne 0 ]
then
  echo "Error: El programa no requereix arguments."
  echo "Usage: $0"
  exit $ERR_NARGS
fi

while read -r line
do
  echo $line | cut -c1-50
done

exit 0
