#! /bin/bash
# Jorge D. Pérez López
# 19/02/2024 
# Exercicis d'scripts bàsics 2
# 
# Sinopsis: Mostar els arguments rebuts línia a línia, tot numerànt-los.
#------------------------------------------------------------------------
ERR_NARGS=1
num=1

if [ $# -lt 1 ]
then
  echo "Error: número d'arguments incorrecte."
  echo "El programa ha de tenir al menys un argument"
  echo "Usage: $0 arg1 arg2 ..."
  exit $ERR_NARGS
fi

for arg in $*
do
  echo "$num: $arg"
  ((num++))
done

exit 0
