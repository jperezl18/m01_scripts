#! /bin/bash
# Jorge D. Pérez López
# 19/02/2024 
# Exercicis d'scripts bàsics 4
# 
# Sinopsis: Fer un programa que rep com a arguments números de més (un o mes) i indica per
# a cada mes rebut quants dies té el més.
#------------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -lt 1 ]
then
  echo "Error: número d'arguments incorrecte."
  echo "El programa ha de tenir al menys un argument"
  echo "Usage: $0 arg1 arg2 ..."
  exit $ERR_NARGS
fi

for mes in $*
do
  case $mes in
    "2")
      echo "El mes $mes té 28 dies";;
    "4"|"6"|"9"|"11")
      echo "El mes $mes té 30 dies";;
    "1"|"3"|"5"|"7"|"8"|"10"|"12")
      echo "El mes $mes té 31 dies";;
    *)
      echo "$mes no és un mes vàlid" >> /dev/stderr;;
  esac
done

exit 0


