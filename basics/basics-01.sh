#! /bin/bash
# Jorge D. Pérez López
# 19/02/2024 
# Exercicis d'scripts bàsics 1
# 
# Sinopsis: Mostrar l'entrada standard linia a linia
#----------------------------------------------------
ERR_NARGS=1
num=1

if [ $# -ne 0 ]
then
  echo "Error: El programa no requereix arguments."
  echo "Usage: $0"
  exit $ERR_NARGS
fi

while read -r line
do
  echo "$num: $line"
  ((num++))
done

exit 0
