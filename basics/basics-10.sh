#! /bin/bash
# Jorge D. Pérez López
# 21/02/2024 
# Exercicis d'scripts bàsics 10
# 
# Sinopsis: Fer un programa que rep com a argument un número indicatiu del número màxim de
# línies a mostrar. El programa processa stdin línia a línia i mostra numerades un
# màxim de num línies.
#------------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -ne 1 ]
then
  echo "Error: El programa ha de tenir exactament un argument"
  echo "Usage: $0 num"
  exit $ERR_NARGS
fi

max=$1
compt=1

read -r line
while [ $max -gt 0 ]
do
  echo "$compt: $line"
  read -r line
  ((compt++))
  ((max--))
done

exit 0
