#! /bin/bash
# Jorge D. Pérez López
# 19/02/2024 
# Exercicis d'scripts bàsics 3
# 
# Sinopsis: Fer un comptador des de zero fins al valor indicat per l’argument rebut.
#---------------------------------------------------------------------------------------
ERR_NARGS=1
num=1

if [ $# -ne 1 ]
then
  echo "Error: número d'arguments incorrecte."
  echo "El programa ha de tenir exactament un argument"
  echo "Usage: $0 arg"
  exit $ERR_NARGS
fi

limit=$1

while [ $num -le $limit ]
do
  echo $num
  ((num++))
done

exit 0
