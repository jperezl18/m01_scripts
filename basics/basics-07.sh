#! /bin/bash
# Jorge D. Pérez López
# 21/02/2024 
# Exercicis d'scripts bàsics 7
# 
# Sinopsis: Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la
# mostra, si no no.
#------------------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -ne 0 ]
then
  echo "Error: El programa no requereix arguments."
  echo "Usage: $0"
  exit $ERR_NARGS
fi

while read -r line
do
  chars=$(echo "$line" | wc -c)
  if [ $chars -gt 60 ] ; then
    echo $line
  fi
done

exit 0
