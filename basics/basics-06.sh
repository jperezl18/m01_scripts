#! /bin/bash
# Jorge D. Pérez López
# 19/02/2024 
# Exercicis d'scripts bàsics 6
# 
# Sinopsis: Fer un programa que rep com a arguments noms de dies de la setmana i mostra
# quants dies eren laborables i quants festius. Si l’argument no és un dia de la
# setmana genera un error per stderr.
#----------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -lt 1 ]
then
  echo "Error: El programa ha de tenir al menys un argument"
  echo "Usage: $0 arg1 arg2 ..."
  exit $ERR_NARGS
fi

n_festius=0
n_laborables=0

for dia in $*
do
  case $dia in
    "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
      ((n_laborables++));;
    "dissabte"|"diumenge")
      ((n_festius++));;
    *)
      echo "$dia no és un dia vàlid" >> /dev/stderr;;
  esac
done

echo "Dies laborables: $n_laborables"
echo "Dies festius: $n_festius"

exit 0
