#! /bin/bash
# Jorge D. Pérez López
# 21/02/2024 
# Exercicis d'scripts bàsics 9
# 
# Sinopsis: Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el
# sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra
# per stderr.
#------------------------------------------------------------------------------------------
while read -r line
do
  grep -q "^$line:" /etc/passwd
  if [ $? -eq 0 ] ; then
    echo $line
  else
    echo "Usuari $line no existeix al sistema" >> /dev/stderr
  fi
done

exit 0

