#! /bin/bash
# Jorge D. Pérez López
# 21/02/2024 
# Exercicis d'scripts bàsics 8
# 
# Sinopsis: Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema
# (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per
# stderr.
#------------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -lt 1 ]
then
  echo "Error: El programa ha de tenir al menys un argument"
  echo "Usage: $0 arg1 arg2 ..."
  exit $ERR_NARGS
fi

for user in $*
do
  grep -q "^$user:" /etc/passwd
  if [ $? -eq 0 ] ; then
    echo $user
  else
    echo "Usuari $user no existeix al sistema" >> /dev/stderr
  fi
done

exit 0
