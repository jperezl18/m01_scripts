#! /bin/bash
# Jorge D. Pérez López
# 04/03/2024 
# Altres exemples d'scripts
# 
# Sinopsis: Processa matrícules que estan a un fitxer. Les matrícules son del format AAAA999.
# Si és vàlida la mostra per stdout, sinó per stderr.
#-------------------------------------------------------------------------------------------------
ERR_NARGS=1
ERR_FILE=2

if [ $# -ne 1 ]
then
  echo "Error: El programa ha de tenir exactament un argument"
  echo "Usage: $0 file"
  exit $ERR_NARGS
fi

if ! [ -f $1 ] ; then
  echo "Error: $1 no és un fitxer"
  echo "Usage: $0 file"
  exit $ERR_FILE
fi

fileIn=$1
status=0

while read -r matricula
do
  echo $matricula | grep -E -i "^[A-Z]{4}[0-9]{3}$"
  if [ $? -ne 0 ] ; then
    echo "Error: $matricula no vàlida." >> /dev/stderr
    status=3
  fi
done < $fileIn

exit $status
