#! /bin/bash
# Jorge D. Pérez López
# 04/06/2024
#
# Sinopsis: pujarGit.sh repositori
#
# Descripció: El programa rep la ruta d'un repositori com a argument i el puja.
# Per defecte al missatge del commit introduïrà la data actual.
#--------------------------------------------------------------------------------------

ERR_NARGS=1
ERR_REPO=2

if [ $# -ne 1 ] ; then
  echo "Error: El programa requereix un argument."
  echo "Usage: $0 repositori"
  exit $ERR_NARGS
fi

repo=$1

data=$(date +%d-%m-%y)

if [ -e $repo ] ; then
  cd $repo
  git add . ; git commit -m "$data" ; git push
  echo "Repositori $repo pujat correctament."
else
  echo "Error: No existeis el repositori $repo."
  exit $ERR_REPO
fi

exit 0
