#! /bin/bash
# Jorge D. Pérez López
# 10/05/2024
#
# Sinopsis: clonarGit.sh
#
# Descripció: El programa mostra un menú amb opcions per clonar diferents repositoris.
# Si el repositori existeix en el directori actual, l'esborra i el torna a clonar.
#--------------------------------------------------------------------------------------

ERR_NARGS=1

if [ $# -ne 0 ] ; then
  echo "Error: El programa no requereix argumens."
  echo "Usage: $0"
  exit $ERR_NARGS
fi

echo "Quin repositori vols clonar?"
echo "1) M01-Scripts"
echo "2) M02-BD"
echo "3) M03-Python"
echo "4) M05-Docker"
echo "5) M10-ADMBD"
echo "6) Sortir"

while read -r opcio
do

  case $opcio in
    "1")
      dir="./m01_scripts"

      if [ -e $dir ] ; then
        rm -rf $dir
      fi

      git clone https://gitlab.com/jperezl18/m01_scripts

      if [ $? -eq 0 ] ; then
        echo "Repositori m01_scripts clonat correctament."
      else
        echo "Error clonant repositori m01_scripts."
      fi;;

    "2")
      dir="./m02_bd"

      if [ -e $dir ] ; then
        rm -rf $dir
      fi

      git clone https://gitlab.com/jperezl18/m02_bd
      
      if [ $? -eq 0 ] ; then
        echo "Repositori m02_bd clonat correctament."
      else
        echo "Error clonant repositori m02_bd."
      fi;;

    "3")
      dir="./m03_python"

      if [ -e $dir ] ; then
        rm -rf $dir
      fi

      git clone https://gitlab.com/jperezl18/m03_python
      
      if [ $? -eq 0 ] ; then
        echo "Repositori m03_python clonat correctament."
      else
        echo "Error clonant repositori m03_python."
      fi;;

    "4")
      dir="./m05_docker"

      if [ -e $dir ] ; then
        rm -rf $dir
      fi

      git clone https://gitlab.com/jperezl18/m05_docker
      
      if [ $? -eq 0 ] ; then
        echo "Repositori m05_docker clonat correctament."
      else
        echo "Error clonant repositori m05_docker."
      fi;;

    "5")
      dir="./m10_admbd"

      if [ -e $dir ] ; then
        rm -rf $dir
      fi

      git clone https://gitlab.com/jperezl18/m10_admbd
      
      if [ $? -eq 0 ] ; then
        echo "Repositori m10_admbd clonat correctament."
      else
        echo "Error clonant repositori m10_admbd."
      fi;;

    "6")
      echo "Adéu!"
      exit 0;;

    *)
      echo "Opció no vàlida.";;
  esac

  echo "Quin repositori vols clonar?"
  echo "1) M01-Scripts"
  echo "2) M02-BD"
  echo "3) M03-Python"
  echo "4) M05-Docker"
  echo "5) M10-ADMBD"
  echo "6) Sortir"

done

