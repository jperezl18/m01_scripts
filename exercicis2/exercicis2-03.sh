#! /bin/bash
# Jorge D. Pérez López
# 26/02/2024 
# Exercicis d'scripts 2 - 03
# 
# Sinopsis: Processar arguments que són matricules:
# a) Llistar les vàlides, del tipus: 9999-AAA.
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el
#    número d’errors (de no vàlides).
#------------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -eq 0 ]
then
  echo "Error: El programa ha de tenir mínim un argument"
  echo "Usage: $0 matrícula1 matrícula2 ..."
  exit $ERR_NARGS
fi

err=0

for matricula in $*
do
  echo $matricula | grep -q -E "^[0-9]{4}-[A-Z]{3}$"
  if [ $? -eq 0 ] ; then
    echo $matricula
  else
    echo "Error: $matricula no vàlida" >> /dev/stderr
    ((err++))
  fi
done

exit $err
