#! /bin/bash
# Jorge D. Pérez López
# 29/02/2024 
# Exercicis d'scripts 2 - 05
# 
# Sinopsis: Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#-------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -ne 0 ]
then
  echo "Error: El programa no requereix arguments"
  echo "Usage: $0"
  exit $ERR_NARGS
fi

while read -r line
do
  echo $line | grep -E "^.{,49}$"
done

exit 0
