#! /bin/bash
# Jorge D. Pérez López
# 29/02/2024 
# Exercicis d'scripts 2 - 06
# 
# Sinopsis: Processar per stdin línies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia
# en format → T. Snyder.
#-------------------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -ne 0 ]
then
  echo "Error: El programa no requereix arguments"
  echo "Usage: $0"
  exit $ERR_NARGS
fi

while read -r line
do
  inicial=$(echo $line | cut -c1)
  cognom=$(echo $line | cut -d" " -f2)
  echo "$inicial. $cognom"
done

exit 0
