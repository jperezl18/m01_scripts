#! /bin/bash
# Jorge D. Pérez López
# 29/02/2024 
# Exercicis d'scripts 2 - 04
# 
# Sinopsis: Processar stdin mostrant per stdout les línies numerades i en majúscules.
#------------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -ne 0 ]
then
  echo "Error: El programa no requereix arguments"
  echo "Usage: $0"
  exit $ERR_NARGS
fi

num=1

while read -r line
do
  echo "$num: $line" | tr 'a-z' 'A-Z'
  ((num++))
done

exit 0

