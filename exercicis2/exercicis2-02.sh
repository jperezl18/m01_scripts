#! /bin/bash
# Jorge D. Pérez López
# 26/02/2024 
# Exercicis d'scripts 2 - 02
# 
# Sinopsis: Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
#------------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -eq 0 ]
then
  echo "Error: El programa ha de tenir mínim un argument"
  echo "Usage: $0 arg1 arg2 ..."
  exit $ERR_NARGS
fi

compt=0

for arg in $*
do
  echo $arg | grep -q -E "^.{3,}"
  if [ $? -eq 0 ] ; then
    ((compt++))
  fi
done

echo "Paraules amb 3 chars o més: $compt"

exit 0
