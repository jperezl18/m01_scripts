#! /bin/bash
# Jorge D. Pérez López
# 26/02/2024 
# Exercicis d'scripts 2 - 01
# 
# Sinopsis: Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
#------------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -eq 0 ]
then
  echo "Error: El programa ha de tenir mínim un argument"
  echo "Usage: $0 arg1 arg2 ..."
  exit $ERR_NARGS
fi

for arg in $*
do
  echo $arg | grep -E "^.{4,}"
done

exit 0
