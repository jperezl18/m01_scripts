#! /bin/bash
# Jorge D. Pérez López
# 31/01/2024
#
# Exemples ordre if
#--------------------------
#
# 1) Validar arguments
if [ $# -ne 1 ]
then
  echo "Error: númro d'argumets incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi

# 2) Xixa
edat=$1

if [ $edat -lt 18 ]
then
  echo "edat $edat és menor d'edat"
elif [ $edat -lt 65 ]
then
  echo "edat $edat és edat activa"
else
  echo "edat $edat és edat de jubilació"
fi
exit 0
