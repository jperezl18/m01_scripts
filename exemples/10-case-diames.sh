#! /bin/bash
# Jorge D. Pérez López
# 05/02/2024 
#
# Descripció: dir els dies que té un més.
# Sinopsis: prog mes
#    a) validar rep un arg
#    b) validar mes [1-12]
#    c) xixa
#------------------------------------------------
#
ERR_NARGS=1
ERR_MES=2
# a)
if [ $# -ne 1 ]
then
  echo "Error: número d'arguments incorrecte."
  echo "$0 ha de tenir exactament 1 argument."
  exit $ERR_NARGS
fi

# b)
mes=$1
if [ $mes -lt 1 -o $mes -gt 12 ] ; then
  echo "Error: $mes no és un número de mes."
  echo "$0 El mes ha de ser de 1 a 12."
  exit $ERR_MES
fi

# c)
case $mes in
  "2")
    echo "El mes té 28 dies";;
  "1"|"3"|"5"|"7"|"8"|"10"|"12")
    echo "El mes té 31 dies";;
  *)
    echo "El mes té 30 dies";;
esac
exit 0
