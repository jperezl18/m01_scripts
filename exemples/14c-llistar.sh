#! /bin/bash
# Jorge D. Pérez López
# 26/02/2024
#
# per cada element dir si es dir, regular o altra cosa
# ----------------------------------------------------
ERR_NARGS=1
ERR_DIR=2

if [ $# -ne 1 ]
then
  echo "Error: El programa ha de tenir exactament un argument"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

dir=$1

if [ ! -d $dir ]
then
  echo "Error: $dir no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_DIR
fi

llista=$(ls $dir)

for elem in $llista
do
  if [ -f $dir/$elem ] ; then
    echo "$elem és un fitxer regular."
  elif [ -d $dir/$elem ] ; then
    echo "$elem és un directori."
  else
    echo "$elem és una altra cosa."
  fi
done

exit 0
