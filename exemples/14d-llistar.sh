#! /bin/bash
# Jorge D. Pérez López
# 26/02/2024
#
# es reben n directoris
# ----------------------------------------------------
ERR_NARGS=1
ERR_DIR=2

if [ $# -eq 0 ]
then
  echo "Error: El programa ha de tenir mínim un argument"
  echo "Usage: $0 dir1 dir2 ..."
  exit $ERR_NARGS
fi

dir=$1

for dir in $*
do
  if [ ! -d $dir ]
  then
    echo "Error: $dir no és un directori" >> /dev/stderr
  else
    llista=$(ls $dir)

    echo "LListat: $dir ------------"
    for elem in $llista
    do
      if [ -f $dir/$elem ] ; then
        echo -e "\t$elem és un fitxer regular."
      elif [ -d $dir/$elem ] ; then
        echo -e "\t$elem és un directori."
      else
        echo -e "\t$elem és una altra cosa."
      fi
    done
  fi
done

exit 0
 


