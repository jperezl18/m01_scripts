#! /bin/bash
# Jorge D. Pérez López
# 26/02/2024
#
# llistar numerant els elements del dir
# -----------------------------------------
ERR_NARGS=1
ERR_DIR=2

if [ $# -ne 1 ]
then
  echo "Error: El programa ha de tenir exactament un argument"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

dir=$1

if [ ! -d $dir ]
then
  echo "Error: $dir no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_DIR
fi

llista=$(ls $dir)
num=1

for elem in $llista
do
  echo "$num: $elem"
  ((num++))
done

exit 0
