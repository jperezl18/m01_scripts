#! /bin/bash
# Jorge D. Pérez López
# 20/03/2023
#
# funcions
#--------------------------

function showAllShells(){
  MIN=$1
  llista_shells=$(cut -d: -f7 /etc/passwd | sort -u)
  for shell in $llista_shells
  do
    num_users=$(grep -E ":$shell$" /etc/passwd | wc -l)
    if [ $num_users -gt $MIN ] ; then
      echo "$shell ($num_users) :"
      grep -E ":$shell$" /etc/passwd | cut -d: -f1 | sed -r 's/^(.*)$/\t\1/'
      echo -e "\n"
    fi
  done
}

function showUsersInGroupByGid() {
  gid=$1
  line=$(grep -E "^[^:]*:[^:]*:$gid:" /etc/group)
  if [ -z "$line" ] ; then
    echo "error..."
    return 1
  fi
  gname=$(echo $line | cut -d: -f1)
  echo "Grup: $gname"
  grep -E "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,7 
}

function showUserByLogin(){
  login=$1
  line=$(grep "^$login:" /etc/passwd)
  if [ -z "$line" ] ; then
    echo "error..."
    return 1
  fi
  uid=$(echo $line | cut -d: -f3)
  gid=$(echo $line | cut -d: -f4)
  shell=$(echo $line | cut -d: -f7)
  echo "login: $login"
  echo "uid: $uid"
  echo "gid: $gid"
  echo "shell: $shell"
  return 0
}

function suma(){
  echo $(($1+$2))
  return 0
}

#rep un login
function showUser(){
  login=$1
  grep "^$login:" /etc/passwd | cut -d: -f1,3,4,7
}

function hola(){
  echo hola
  exit 0
}

function dia(){
  date
  return 0
}

function suma(){
  echo $(( $1 + $2 ))
  return 0
}

