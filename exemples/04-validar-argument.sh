#! /bin/bash
# Jorge D. Pérez López
# 05/02/2024 
#
#Validar que té exàctasment 2 args i mostrar-los
#nom cognom
#------------------------------------------------
#
if [ $# -ne 2 ]
then
  echo "Error: número d'arguments incorrecte."
  echo "Han d'haver exactament dos arguments."
  exit 1
fi

nom=$1
cognom=$2

echo "nom: $nom"
echo "cognom: $cognom"
exit 0
