#! /bin/bash
# Jorge D. Pérez López
# 14/02/2024 
#
# Descripció: exemples while
#-----------------------------------
# 8) Processar linia a linia un file
fileIn=$1
num=1

while read -r line
do
  chars=$(echo $line | wc -c)
  echo "$num: ($chars) $line" | tr 'a-z' 'A-Z'
  ((num++))
done < $fileIn
exit 0

# 7) Numerar i mostrar en majúscula
num=1

while read -r line
do
  echo "$num: $line" | tr 'a-z' 'A-Z'
  ((num++))
done
exit 0


# 6) Itera linia linia fins a token (per exemple FI)
TOKEN="FI"
num=1

read -r line
while [ "$line" != $TOKEN ]
do
  echo "$num: $line"
  ((num++))
  read -r line  
done
exit 0

# 5) numerar les linies rebudes
num=1

while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0

# 4) processar stdin linia a linia
while read -r line
do
  echo $line
done
exit 0

# 3) Iterar la llista d'arguments (ojo! usar normalment for)
while [ -n "$1" ]
do
  echo "$1 $# $*"
  shift
done
exit 0

# 2) comptador decrementa valor rebut
MIN=0
num=$1

while [ $num -ge $MIN ]
do
  echo $num
  ((num--))
done
exit 0

# 1) mostrar números de l'1 al 10
MAX=10
num=1

while [ $num -le $MAX ]
do
  echo -n "$num "
  ((num++))
done
exit 0
