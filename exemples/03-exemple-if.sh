#! /bin/bash
# Jorge D. Pérez López
# 31/01/2024
#
# Exemples ordre if
#--------------------------
#
# 1) Validar arguments
if [ $# -ne 1 ]
then
  echo "Error: númro d'argumets incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi

edat=$1

if [ $edat -ge 18 ]
then
  echo "edat $edat és major d'edat"
fi
exit 0
