#! /bin/bash
# Jorge D. Pérez López
# 05/02/2024 
#
# llistar el directori rebut
#         a) verificar rep un argument
#         b) verificar que és un directori
#------------------------------------------------
#
ERR_NARGS=1
ERR_DIR=2

if [ $# -ne 1 ]
then
  echo "Error: número d'arguments incorrecte."
  echo "$0 ha de tenir exactament un argument"
  exit $ERR_NARGS
fi

if ! [ -d $1 ]
then
  echo "Error: $1 no és un directori."
  echo "L'Argument ha de ser un directori"
  exit $ERR_DIR
fi

dir=$1

ls -l $dir

exit 0
