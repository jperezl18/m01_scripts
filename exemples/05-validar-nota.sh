#! /bin/bash
# Jorge D. Pérez López
# 05/02/2024 
#
#Validar nota: suspès, aprovat
#         a) rep un argument
#         b) és del 0 al 10
#------------------------------------------------
#
ERR_NARGS=1
ERR_NOTA=2

if [ $# -ne 1 ]
then
  echo "Error: número d'arguments incorrecte."
  echo "$0 ha de tenir exactament un argument"
  exit $ERR_NARGS
fi

if [ $1 -lt 0 -o $1 -gt 10 ]
then
  echo "Error: Valor de nota $1 invàlid."
  echo "La nota ha de ser un nombre entre 0 i 10."
  exit $ERR_NOTA
fi

nota=$1

if [ $nota -lt 5 ]
then
  echo "La nota $nota és un suspés."
else
  echo "La nota $nota és aprovat."
fi

exit 0
