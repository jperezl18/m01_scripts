#! /bin/bash
# Jorge D. Pérez López
# 26/02/2024
#
# rep un arg i és un directori i es llista
# -----------------------------------------
ERR_NARGS=1
ERR_DIR=2

if [ $# -ne 1 ]
then
  echo "Error: El programa ha de tenir exactament un argument"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

dir=$1

if [ ! -d $dir ]
then
  echo "Error: $dir no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_DIR
fi

ls $dir

exit 0
