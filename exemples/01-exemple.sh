#! /bin/bash
# Jorge D. Pérez López
# 29/01/2024
#
# Exemple de primer programa
#  shebang
#  capçalera
#  -------------------------------------------
echo "hola mon"
nom="pere pou prat"
edat=25
echo $nom $edat
echo "$nom $edat"
echo '$nom $edat'
uname -a
#uptime
#ps
echo $SHELL
echo $SHLVL
echo $((4*32))
echo $((edat*2))

read nom edat
echo -e "nom:$nom\n edat:$edat\n"
exit 0
