#! /bin/bash
# Jorge D. Pérez López
# 05/02/2024 
#
#Validar nota: suspès, aprovat, notable i excel·lent
#         a) rep un argument
#         b) és del 0 al 10
#------------------------------------------------
#
ERR_NARGS=1
ERR_NOTA=2

if [ $# -ne 1 ]
then
  echo "Error: número d'arguments incorrecte."
  echo "$0 ha de tenir exactament un argument"
  exit $ERR_NARGS
fi

if [ $1 -lt 0 -o $1 -gt 10 ]
then
  echo "Error: Valor de nota $1 invàlid."
  echo "La nota ha de ser un nombre entre 0 i 10."
  exit $ERR_NOTA
fi

nota=$1

if [ $nota -lt 5 ]
then
  echo "La nota $nota és un suspés."
elif [ $nota -lt 7 ]
then
  echo "La nota $nota és aprovat."
elif [ $nota -lt 9 ]
then
  echo "La nota $nota és un notable."
else
  echo "La nota $nota és excel·lent."
fi

exit 0
