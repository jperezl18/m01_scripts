#! /bin/bash
# Jorge D. Pérez López
# 05/02/2024 
#
# Indicar si dir és: regular, dir, link o altra cosa
#------------------------------------------------
#
ERR_NARGS=1

if [ $# -ne 1 ]
then
  echo "Error: número d'arguments incorrecte."
  echo "$0 ha de tenir exactament un argument"
  exit $ERR_NARGS
fi

file=$1

if [ -f $file ] ; then
  echo "$file és un regular file."
elif [ -d $file ] ; then
  echo "$file és un directori."
elif [ -h $file ] ; then
  echo "$file és un link."
else
  echo "$file és una altra cosa."
fi

exit 0
