#! /bin/bash
# Jorge D. Pérez López
# 07-02-2024
#
# Exemple case
# ------------------------
#
# 2) dl dt dc dj dv -------> laborables
# ds dm -> festius
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
    echo "$1 és un dia laborable";;
  "ds"|"dm")
    echo "$1 és un dia festiu";;
  *)
    echo "això ($1) no és un dia";;
esac
exit 0

# 1) exemple vocals
case $1 in
  [aeiou])
    echo "$1 és una vocal"
    ;;
  [bcdfghjklmnpqrstvwxyz])
    echo "$1 és una consonant"
    ;;
  *)
    echo "$1 és una altra cosa"
    ;;
esac

exit 0
