#! /bin/bash
# Jorge D. Pérez López
# 14/02/2024 
#
# Descripció: for-notes nota...
# 	suspés, aprovat, excel·lent
#-----------------------------------
# 1) Validar arguments
if [ $# -eq 0 ]; then
  echo "Error: número d'argumets incorrecte"
  echo "Usage: $0 nota..."
  exit 1
fi

# 2) iterar la llista d'argumets
for nota in $*
do
  if ! [ $nota -ge 0 -a $nota -le 10 ]; then
    echo "Error: nota $nota no vàlida (0-10)" >> /dev/stderr
  else
    if [ $nota -lt 5 ]; then
      echo "$nota és suspés"
    elif [ $nota -lt 7 ]; then
      echo "$nota és aprovat"
    elif [ $nota -lt 9 ]; then
      echo "$nota és notable"
    else
      echo "$nota és excel·lent"
    fi
  fi
done
exit 0
